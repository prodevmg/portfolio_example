export var phpSingleton = `<pre style="background:#000;color:#f8f8f8">&lt;?php

<span style="color:#99cf50">class</span> <span style="text-decoration:underline">Config</span> <span style="color:#99cf50">implements</span> <span style="color:#9b5c2e;font-style:italic">ArrayAccess
</span>{
    <span style="color:#99cf50">protected</span> <span style="color:#99cf50">static</span> <span style="color:#3e87e3">$instance</span> <span style="color:#e28964">=</span> <span style="color:#3387cc">null</span>;
    <span style="color:#99cf50">protected</span> <span style="color:#3e87e3">$_data</span><span style="color:#e28964">=</span> <span style="color:#dad085">array</span>();
    
    <span style="color:#aeaeae;font-style:italic">/**
     * Call this method to get singleton
     *
     * <span style="color:#e28964">@return</span> <span style="color:#e28964">@var</span>
     */</span>
    <span style="color:#99cf50">public static </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">singleton</span>()
    {
<span style="color:#e28964">        if</span> (<span style="color:#e28964">!</span><span style="color:#dad085">isset</span>(<span style="color:#99cf50">static</span><span style="color:#e28964">::</span><span style="color:#3e87e3">$instance</span>)) {
            <span style="color:#99cf50">static</span><span style="color:#e28964">::</span><span style="color:#3e87e3">$instance</span> <span style="color:#e28964">=</span> <span style="color:#e28964">new</span> <span style="color:#99cf50">static</span>;
        }
<span style="color:#e28964">        return</span> <span style="color:#99cf50">static</span><span style="color:#e28964">::</span><span style="color:#3e87e3">$instance</span>;
    }

    <span style="color:#99cf50">protected </span><span style="color:#99cf50">function</span> <span style="color:#dad085">__construct</span>()
    {
        <span style="color:#aeaeae;font-style:italic">// No constructor!</span>
    }

    <span style="color:#99cf50">protected </span><span style="color:#99cf50">function</span> <span style="color:#dad085">__clone</span>()
    {
        <span style="color:#aeaeae;font-style:italic">//No clones!</span>
    }
    
    <span style="color:#aeaeae;font-style:italic">/**
     * Array Accessor Functions
     * 
     **/</span>
     
    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">offsetSet</span>(<span style="color:#3e87e3">$offset</span>, <span style="color:#3e87e3">$value</span>) {
<span style="color:#e28964">        if</span> (<span style="color:#dad085">is_null</span>(<span style="color:#3e87e3">$offset</span>)) {
            <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data[] <span style="color:#e28964">=</span> <span style="color:#3e87e3">$value</span>;
        }<span style="color:#e28964"> else</span> {
            <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data[<span style="color:#3e87e3">$offset</span>] <span style="color:#e28964">=</span> <span style="color:#3e87e3">$value</span>;
        }
    }

    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">offsetExists</span>(<span style="color:#3e87e3">$offset</span>) {
<span style="color:#e28964">        return</span> <span style="color:#dad085">isset</span>(<span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data[<span style="color:#3e87e3">$offset</span>]);
    }

    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">offsetUnset</span>(<span style="color:#3e87e3">$offset</span>) {
        <span style="color:#dad085">unset</span>(<span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data[<span style="color:#3e87e3">$offset</span>]);
    }

    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">offsetGet</span>(<span style="color:#3e87e3">$offset</span>) {
<span style="color:#e28964">        return</span> <span style="color:#dad085">isset</span>(<span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data[<span style="color:#3e87e3">$offset</span>]) ? <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data[<span style="color:#3e87e3">$offset</span>] : <span style="color:#3387cc">null</span>;
    }
    
    <span style="color:#aeaeae;font-style:italic">/**
     * End Array Accessor
     * 
     **/</span>
     
     
     <span style="color:#aeaeae;font-style:italic">/**
     * Mutator/ Magic Methods
     * 
     **/</span>
 
    <span style="color:#aeaeae;font-style:italic">/**
     * Call this method to set a single property via object accessor
     *
     * <span style="color:#e28964">@return</span> bool
     */</span>
    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#dad085">__set</span>(<span style="color:#3e87e3">$property</span>, <span style="color:#3e87e3">$value</span>) {
            
            <span style="color:#aeaeae;font-style:italic">// No direct setting of data</span>
<span style="color:#e28964">        if</span>(<span style="color:#3e87e3">$property</span> <span style="color:#e28964">===</span> <span style="color:#65b042">'_data'</span>) {
<span style="color:#e28964">            return</span> <span style="color:#3387cc">false</span>;
        }<span style="color:#e28964"> else</span> {
<span style="color:#e28964">            return</span> <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>set(<span style="color:#3e87e3">$property</span>, <span style="color:#3e87e3">$value</span>);
            }
     
    }
    
    <span style="color:#aeaeae;font-style:italic">/**
     * Call this method to unset a single property
     *
     * <span style="color:#e28964">@return</span> bool
     */</span>
    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#dad085">__unset</span>(<span style="color:#3e87e3">$property</span>) {
            
            <span style="color:#aeaeae;font-style:italic">// No direct setting of data</span>
<span style="color:#e28964">        if</span>(<span style="color:#3e87e3">$property</span> <span style="color:#e28964">===</span> <span style="color:#65b042">'_data'</span>) {
<span style="color:#e28964">            return</span> <span style="color:#3387cc">false</span>;
        }<span style="color:#e28964"> else</span> {
            <span style="color:#dad085">unset</span>(<span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data[<span style="color:#3e87e3">$property</span>]);
<span style="color:#e28964">            return</span> <span style="color:#3387cc">true</span>;
            }
     
    }
     
     <span style="color:#aeaeae;font-style:italic">/**
     * Call this method to set a get a property via object accessors
     *
     * <span style="color:#e28964">@return</span> bool
     */</span>
    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#dad085">__get</span>(<span style="color:#3e87e3">$property</span>) {
        
<span style="color:#e28964">        if</span>(<span style="color:#3e87e3">$property</span> <span style="color:#e28964">===</span> <span style="color:#65b042">'_data'</span>) {
<span style="color:#e28964">            return</span> <span style="color:#3387cc">false</span>;
        }<span style="color:#e28964"> else</span> {
<span style="color:#e28964">            return</span> <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>get(<span style="color:#3e87e3">$property</span>);
            }
                
        
    }
    
    <span style="color:#aeaeae;font-style:italic">/**
     * This method encodes the config list as json
     *
     * <span style="color:#e28964">@return</span> string
     */</span>
    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">__toString</span>()
    {
<span style="color:#e28964">        return</span> <span style="color:#dad085">json_encode</span>(<span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data);
    }
    
    <span style="color:#aeaeae;font-style:italic">/**
     * Dynamic Function to check whether key exists
     * 
     * <span style="color:#e28964">@return</span> bool
     **/</span>
     
     <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#dad085">__call</span>(<span style="color:#3e87e3">$name</span>, <span style="color:#3e87e3">$arguments</span>) {
        
        <span style="color:#3e87e3">$endsWithPostfix</span> <span style="color:#e28964">=</span> <span style="color:#3387cc">false</span>;
        <span style="color:#3e87e3">$postfix</span> <span style="color:#e28964">=</span> <span style="color:#65b042">'Exists'</span>;
        <span style="color:#3e87e3">$length</span> <span style="color:#e28964">=</span> <span style="color:#dad085">strlen</span>(<span style="color:#3e87e3">$postfix</span>);
        
        <span style="color:#3e87e3">$prefixLength</span> <span style="color:#e28964">=</span> <span style="color:#dad085">strlen</span>(<span style="color:#3e87e3">$name</span>) <span style="color:#e28964">-</span> <span style="color:#3e87e3">$length</span>;
        <span style="color:#3e87e3">$prefix</span> <span style="color:#e28964">=</span> <span style="color:#dad085">substr</span>(<span style="color:#3e87e3">$name</span>,<span style="color:#3387cc">0</span>,<span style="color:#3e87e3">$prefixLength</span>);
        
        
        
<span style="color:#e28964">        if</span> (<span style="color:#3e87e3">$length</span> <span style="color:#e28964">==</span> <span style="color:#3387cc">0</span>) {
<span style="color:#e28964">            return</span> <span style="color:#3387cc">true</span>;
        }
        
        <span style="color:#3e87e3">$endsWithPostfix</span> <span style="color:#e28964">=</span> (<span style="color:#dad085">substr</span>(<span style="color:#3e87e3">$name</span>, <span style="color:#e28964">-</span><span style="color:#3e87e3">$length</span>) <span style="color:#e28964">===</span> <span style="color:#3e87e3">$postfix</span>);
        
<span style="color:#e28964">        if</span>(<span style="color:#3e87e3">$endsWithPostfix</span>) <span style="color:#aeaeae;font-style:italic">// check if name ends with Exists</span>
        {
            
<span style="color:#e28964">            return</span> <span style="color:#dad085">array_key_exists</span>(<span style="color:#3e87e3">$prefix</span>,<span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data);
        }
<span style="color:#e28964">        else</span>
            <span style="color:#e28964">throw</span> <span style="color:#e28964">new</span> <span style="color:#9b859d">Exception</span>(<span style="color:#65b042">'Function does not exist.'</span>);
        
    }
    
    <span style="color:#aeaeae;font-style:italic">/**
     * End Mutator/ Magic Methods
     * 
     **/</span>

    <span style="color:#aeaeae;font-style:italic">/**
     * This method inserts a key value pair into the _data variable
     * 
     * <span style="color:#e28964">@return</span> bool
     **/</span>
    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">set</span>(<span style="color:#3e87e3">$k</span>,<span style="color:#3e87e3">$v</span>)
    {
        <span style="color:#3e87e3">$keys</span> <span style="color:#e28964">=</span> <span style="color:#dad085">array</span>(<span style="color:#3e87e3">$k</span>);
<span style="color:#e28964">        return</span> <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data <span style="color:#e28964">=</span> <span style="color:#dad085">array_fill_keys</span>(<span style="color:#3e87e3">$keys</span>, <span style="color:#3e87e3">$v</span>);
    }
    
    <span style="color:#aeaeae;font-style:italic">/**
     * This method inserts muliple key value pairs into the _data variable
     * 
     * <span style="color:#e28964">@return</span> bool
     **/</span>
    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">setValues</span>(<span style="color:#3e87e3">$arg</span>)
    {
        <span style="color:#aeaeae;font-style:italic">// Checks for Array Notation</span>
<span style="color:#e28964">        if</span>(<span style="color:#dad085">is_array</span>(<span style="color:#3e87e3">$arg</span>)){
<span style="color:#e28964">            return</span> <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data <span style="color:#e28964">=</span> <span style="color:#dad085">array_merge</span>(<span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data,<span style="color:#3e87e3">$arg</span>);
        }
        
        <span style="color:#aeaeae;font-style:italic">// Checks for Object Notation</span>
<span style="color:#e28964">        else</span><span style="color:#e28964"> if</span>(<span style="color:#dad085">is_object</span>(<span style="color:#3e87e3">$arg</span>)){
<span style="color:#e28964">            return</span> <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data <span style="color:#e28964">=</span> <span style="color:#dad085">array_merge</span>(<span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data,(<span style="color:#99cf50">array</span>)<span style="color:#3e87e3">$arg</span>);
        }
        
<span style="color:#e28964">        else</span> {
            <span style="color:#e28964">throw</span> <span style="color:#e28964">new</span> <span style="color:#9b859d">Exception</span>(<span style="color:#65b042">'Not an object or array.'</span>);
        }
        
        

    }
    
    <span style="color:#aeaeae;font-style:italic">/**
     * This method retrieves a key value pair from the _data variable based on key
     * 
     * <span style="color:#e28964">@return</span> bool
     **/</span>
    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">get</span>(<span style="color:#3e87e3">$key</span>, <span style="color:#3e87e3">$default</span> <span style="color:#e28964">=</span> <span style="color:#3387cc">false</span>)
    {
        <span style="color:#3e87e3">$numargs</span> <span style="color:#e28964">=</span> <span style="color:#dad085">func_num_args</span>(); <span style="color:#aeaeae;font-style:italic">// checking the number of arguments</span>
        
<span style="color:#e28964">        if</span>(<span style="color:#dad085">isset</span>(<span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data[<span style="color:#3e87e3">$key</span>]))
<span style="color:#e28964">            return</span> <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data[<span style="color:#3e87e3">$key</span>];
            
<span style="color:#e28964">        else</span><span style="color:#e28964"> if</span>(<span style="color:#e28964">!</span><span style="color:#dad085">isset</span>(<span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>_data[<span style="color:#3e87e3">$key</span>]) <span style="color:#e28964">&amp;&amp;</span> <span style="color:#3e87e3">$numargs</span> <span style="color:#e28964">===</span> <span style="color:#3387cc">2</span>) <span style="color:#aeaeae;font-style:italic">// if the value is not set but default is given</span>
<span style="color:#e28964">            return</span> <span style="color:#3e87e3">$default</span>;
            
<span style="color:#e28964">        else</span> { 
            <span style="color:#e28964">throw</span> <span style="color:#e28964">new</span> <span style="color:#9b859d">Exception</span>(<span style="color:#65b042">'Default nor Value is set.'</span>);
        }
    }
    
    
    <span style="color:#aeaeae;font-style:italic">/**
     * This method loads Json and sets the keys and values into the _data variable
     * 
     * <span style="color:#e28964">@return</span> bool
     **/</span>
    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">loadJson</span>(<span style="color:#3e87e3">$arg</span>){
        <span style="color:#3e87e3">$json</span> <span style="color:#e28964">=</span> <span style="color:#dad085">json_decode</span>(<span style="color:#3e87e3">$arg</span>, <span style="color:#3387cc">true</span>);
<span style="color:#e28964">        return</span> <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>setValues(<span style="color:#3e87e3">$json</span>);
    }
    
    

}
</pre>`;

export var integralWP = `
<pre style="background:#000;color:#f8f8f8">&lt;?php

<span style="color:#aeaeae;font-style:italic">/*
Plugin Name: Integral WP Enhancer
Plugin URI: 
Description: Adding the ability to sync custom merge tags
Author: Awesome Devs
Version: 1.1
Author URI:
*/</span>


add_action(<span style="color:#65b042">'admin_menu'</span>, <span style="color:#65b042">'order_sync_button_menu'</span>);

<span style="color:#99cf50">function</span> <span style="color:#89bdff">order_sync_button_menu</span>(){
  add_menu_page(<span style="color:#65b042">'Integral WP Enhancer'</span>, <span style="color:#65b042">'Integral WP Enhancer'</span>, <span style="color:#65b042">'manage_options'</span>, <span style="color:#65b042">'sync-orders-slug'</span>, <span style="color:#65b042">'integralwp_enhancer_admin_page'</span>);

}

<span style="color:#99cf50">function</span> <span style="color:#89bdff">integralwp_enhancer_admin_page</span>() {

  <span style="color:#aeaeae;font-style:italic">// This function creates the output for the admin page.</span>
  <span style="color:#aeaeae;font-style:italic">// It also checks the value of the $_POST variable to see whether</span>
  <span style="color:#aeaeae;font-style:italic">// there has been a form submission. </span>

  <span style="color:#aeaeae;font-style:italic">// The check_admin_referer is a WordPress function that does some security</span>
  <span style="color:#aeaeae;font-style:italic">// checking and is recommended good practice.</span>

  <span style="color:#aeaeae;font-style:italic">// General check for user permissions.</span>
<span style="color:#e28964">  if</span> (<span style="color:#e28964">!</span>current_user_can(<span style="color:#65b042">'manage_options'</span>))  {
    wp_die( __(<span style="color:#65b042">'You do not have sufficient privileges to access this page.'</span>)    );
  }

  <span style="color:#aeaeae;font-style:italic">// Start building the page</span>

  <span style="color:#dad085">echo</span> <span style="color:#65b042">'&lt;div class="wrap">'</span>;

  <span style="color:#dad085">echo</span> <span style="color:#65b042">'&lt;h2>Manually Sync Last Orders Tag&lt;/h2>'</span>;

  <span style="color:#aeaeae;font-style:italic">// Check whether the button has been pressed AND also check the nonce</span>
<span style="color:#e28964">  if</span> (<span style="color:#dad085">isset</span>(<span style="color:#3e87e3">$_POST</span>[<span style="color:#65b042">'order_sync_button'</span>]) <span style="color:#e28964">&amp;&amp;</span> check_admin_referer(<span style="color:#65b042">'order_sync_button_clicked'</span>)) {
    <span style="color:#aeaeae;font-style:italic">// the button has been pressed AND we've passed the security check</span>
    order_sync_button_action();
  }

  <span style="color:#dad085">echo</span> <span style="color:#65b042">'&lt;form action="options-general.php?page=sync-orders-slug" method="post">'</span>;

  <span style="color:#aeaeae;font-style:italic">// this is a WordPress security feature - see: https://codex.wordpress.org/WordPress_Nonces</span>
  wp_nonce_field(<span style="color:#65b042">'order_sync_button_clicked'</span>);
  <span style="color:#dad085">echo</span> <span style="color:#65b042">'&lt;input type="hidden" value="true" name="order_sync_button" />'</span>;
  submit_button(<span style="color:#65b042">'Sync Orders'</span>);
  <span style="color:#dad085">echo</span> <span style="color:#65b042">'&lt;/form>'</span>;

  <span style="color:#dad085">echo</span> <span style="color:#65b042">'&lt;/div>'</span>;

}

<span style="color:#99cf50">function</span> <span style="color:#89bdff">order_sync_button_action</span>()
{
  <span style="color:#dad085">echo</span> <span style="color:#65b042">'&lt;div id="message" class="updated fade">&lt;p>'</span>
    <span style="color:#e28964">.</span><span style="color:#65b042">'The "Call Function" button was clicked.'</span> <span style="color:#e28964">.</span> <span style="color:#65b042">'&lt;/p>&lt;/div>'</span>;

  <span style="color:#aeaeae;font-style:italic">// Create the WP_User_Query objects</span>
  <span style="color:#3e87e3">$wp_user_query_cust</span> <span style="color:#e28964">=</span> <span style="color:#e28964">new</span> <span style="color:#9b859d">WP_User_Query</span>(<span style="color:#dad085">array</span>(<span style="color:#65b042">'role'</span> <span style="color:#e28964">=></span> <span style="color:#65b042">'customer'</span>));
  <span style="color:#3e87e3">$customer</span> <span style="color:#e28964">=</span> <span style="color:#3e87e3">$wp_user_query_cust</span><span style="color:#e28964">-></span>get_results();
  <span style="color:#3e87e3">$wp_user_query_custl</span> <span style="color:#e28964">=</span> <span style="color:#e28964">new</span> <span style="color:#9b859d">WP_User_Query</span>(<span style="color:#dad085">array</span>(<span style="color:#65b042">'role'</span> <span style="color:#e28964">=></span> <span style="color:#65b042">'customer_lab'</span>));
  <span style="color:#3e87e3">$customer_lab</span> <span style="color:#e28964">=</span> <span style="color:#3e87e3">$wp_user_query_custl</span><span style="color:#e28964">-></span>get_results();
  <span style="color:#3e87e3">$wp_user_query_custa</span> <span style="color:#e28964">=</span> <span style="color:#e28964">new</span> <span style="color:#9b859d">WP_User_Query</span>(<span style="color:#dad085">array</span>(<span style="color:#65b042">'role'</span> <span style="color:#e28964">=></span> <span style="color:#65b042">'customer_all'</span>));
  <span style="color:#3e87e3">$customer_all</span> <span style="color:#e28964">=</span> <span style="color:#3e87e3">$wp_user_query_custa</span><span style="color:#e28964">-></span>get_results();
  <span style="color:#3e87e3">$wp_user_query_custs</span> <span style="color:#e28964">=</span> <span style="color:#e28964">new</span> <span style="color:#9b859d">WP_User_Query</span>(<span style="color:#dad085">array</span>(<span style="color:#65b042">'role'</span> <span style="color:#e28964">=></span> <span style="color:#65b042">'solo_customer'</span>));
  <span style="color:#3e87e3">$solo_customer</span> <span style="color:#e28964">=</span> <span style="color:#3e87e3">$wp_user_query_custs</span><span style="color:#e28964">-></span>get_results();
  <span style="color:#3e87e3">$wp_user_query_custe</span> <span style="color:#e28964">=</span> <span style="color:#e28964">new</span> <span style="color:#9b859d">WP_User_Query</span>(<span style="color:#dad085">array</span>(<span style="color:#65b042">'role'</span> <span style="color:#e28964">=></span> <span style="color:#65b042">'epoc_customer'</span>));
  <span style="color:#3e87e3">$epoc_customer</span> <span style="color:#e28964">=</span> <span style="color:#3e87e3">$wp_user_query_custe</span><span style="color:#e28964">-></span>get_results();

  <span style="color:#aeaeae;font-style:italic">// Get the results</span>
  <span style="color:#aeaeae;font-style:italic">// </span>
  <span style="color:#aeaeae;font-style:italic">//</span>
  <span style="color:#aeaeae;font-style:italic">// </span>
  <span style="color:#aeaeae;font-style:italic">//  </span>
  <span style="color:#3e87e3">$users</span> <span style="color:#e28964">=</span> <span style="color:#dad085">array_merge</span>( <span style="color:#3e87e3">$customer</span>, <span style="color:#3e87e3">$customer_lab</span>, <span style="color:#3e87e3">$customer_all</span>, <span style="color:#3e87e3">$solo_customer</span>, <span style="color:#3e87e3">$epoc_customer</span> );
  <span style="color:#3e87e3">$users_count</span> <span style="color:#e28964">=</span> <span style="color:#3387cc">0</span>;
  <span style="color:#aeaeae;font-style:italic">// Check for results</span>
<span style="color:#e28964">  if</span> (<span style="color:#e28964">!</span><span style="color:#dad085">empty</span>(<span style="color:#3e87e3">$users</span>)) {

      <span style="color:#aeaeae;font-style:italic">// loop trough each author</span>
<span style="color:#e28964">      foreach</span> (<span style="color:#3e87e3">$users</span> <span style="color:#e28964">as</span> <span style="color:#3e87e3">$user</span>)
      {

        <span style="color:#3e87e3">$old_user_data</span> <span style="color:#e28964">=</span> <span style="color:#3e87e3">$user</span>;
          <span style="color:#3e87e3">$dateString</span> <span style="color:#e28964">=</span> get_last_order_date(<span style="color:#3e87e3">$user</span><span style="color:#e28964">-></span>id);


<span style="color:#e28964">          if</span>(<span style="color:#3e87e3">$dateString</span> <span style="color:#e28964">==</span> <span style="color:#65b042">''</span>)
          {
            <span style="color:#3e87e3">$newDateString</span> <span style="color:#e28964">=</span> <span style="color:#65b042">''</span>;
          }
<span style="color:#e28964">          else</span>
          {
            <span style="color:#3e87e3">$newDateString</span> <span style="color:#e28964">=</span> <span style="color:#dad085">date_format</span>(<span style="color:#dad085">date_create_from_format</span>(<span style="color:#65b042">'Y-m-d H:i:s'</span>, <span style="color:#3e87e3">$dateString</span>), <span style="color:#65b042">'m/d/Y'</span>); <span style="color:#aeaeae;font-style:italic">// Convert to mailchimp format </span>
          }



          <span style="color:#aeaeae;font-style:italic">// add points meta all the user's data</span>
          update_user_meta( <span style="color:#3e87e3">$user</span><span style="color:#e28964">-></span>id, <span style="color:#65b042">'last_order'</span>, <span style="color:#3e87e3">$newDateString</span>); <span style="color:#aeaeae;font-style:italic">// Last order date meta</span>

          <span style="color:#3e87e3">$users_count</span> <span style="color:#e28964">+</span><span style="color:#e28964">=</span> <span style="color:#3387cc">1</span>;
          
          <span style="color:#aeaeae;font-style:italic">//- Trigger the plugin user update process </span>
            do_action(<span style="color:#65b042">'integral_mailchimp_user_update'</span>, <span style="color:#3e87e3">$user</span><span style="color:#e28964">-></span>id, <span style="color:#3e87e3">$old_user_data</span>);
            
            <span style="color:#dad085">echo</span> <span style="color:#65b042">'&lt;div id="message" class="updated fade">&lt;p>'</span>
            <span style="color:#e28964">.</span><span style="color:#3e87e3">$user</span><span style="color:#e28964">-></span>id<span style="color:#e28964">.</span><span style="color:#65b042">' User Meta is: '</span> <span style="color:#e28964">.</span>get_user_meta(<span style="color:#3e87e3">$user</span><span style="color:#e28964">-></span>id, <span style="color:#65b042">'last_order'</span>, <span style="color:#3387cc">true</span>)<span style="color:#e28964">.</span> <span style="color:#65b042">'&lt;/p>&lt;/div>'</span>;

    
      }

  }

  <span style="color:#dad085">echo</span> <span style="color:#65b042">'&lt;div id="message" class="updated fade">&lt;p>'</span>
    <span style="color:#e28964">.</span><span style="color:#3e87e3">$users_count</span><span style="color:#e28964">.</span><span style="color:#65b042">' Users Updated.'</span> <span style="color:#e28964">.</span> <span style="color:#65b042">'&lt;/p>&lt;/div>'</span>;
    

} 

<span style="color:#99cf50">function</span> <span style="color:#89bdff">get_last_order_date</span>(<span style="color:#3e87e3">$user_id</span>)
{
  <span style="color:#aeaeae;font-style:italic">// The query arguments</span>
    <span style="color:#3e87e3">$args</span> <span style="color:#e28964">=</span> <span style="color:#dad085">array</span>(
        <span style="color:#aeaeae;font-style:italic">// WC orders post type</span>
        <span style="color:#65b042">'post_type'</span>   <span style="color:#e28964">=></span> <span style="color:#65b042">'shop_order'</span>,
        <span style="color:#aeaeae;font-style:italic">// Only orders with status "completed" (others common status: 'wc-on-hold' or 'wc-processing')</span>
        <span style="color:#65b042">'post_status'</span> <span style="color:#e28964">=></span> <span style="color:#65b042">'wc-completed'</span>, 
        <span style="color:#aeaeae;font-style:italic">// only 1 post</span>
        <span style="color:#65b042">'numberposts'</span> <span style="color:#e28964">=></span> <span style="color:#3387cc">1</span>,
        <span style="color:#aeaeae;font-style:italic">// for current user id</span>
        <span style="color:#65b042">'meta_key'</span>    <span style="color:#e28964">=></span> <span style="color:#65b042">'_customer_user'</span>,
        <span style="color:#65b042">'meta_value'</span>  <span style="color:#e28964">=></span> <span style="color:#3e87e3">$user_id</span>,
    );


    <span style="color:#3e87e3">$latest_order</span> <span style="color:#e28964">=</span> get_posts( <span style="color:#3e87e3">$args</span> );
    

<span style="color:#e28964">    if</span> (<span style="color:#dad085">empty</span>(<span style="color:#3e87e3">$latest_order</span>)) {
       <span style="color:#3e87e3">$latest_order_date</span> <span style="color:#e28964">=</span> <span style="color:#65b042">''</span>;
    }
<span style="color:#e28964">    else</span>{
       <span style="color:#3e87e3">$latest_order_date</span> <span style="color:#e28964">=</span> <span style="color:#3e87e3">$latest_order</span>[<span style="color:#3387cc">0</span>]<span style="color:#e28964">-></span>post_date;
    }

<span style="color:#e28964">    return</span> <span style="color:#3e87e3">$latest_order_date</span>;

}



?>
</pre>
`;

export var postsController = `
<pre style="background:#000;color:#f8f8f8">&lt;?php

<span style="color:#e28964">namespace</span> <span style="text-decoration:underline">App&#92;Http&#92;Controllers</span>;

<span style="color:#e28964">use</span> <span style="color:#9b859d">Illuminate&#92;Http&#92;Request</span>;

<span style="color:#e28964">use</span> <span style="color:#9b859d">App&#92;Post</span>;
<span style="color:#e28964">use</span> <span style="color:#9b859d">App&#92;Repositories&#92;Posts</span>;
<span style="color:#e28964">use</span> <span style="color:#9b859d">Carbon&#92;Carbon</span>;

<span style="color:#99cf50">class</span> <span style="text-decoration:underline">PostsController</span> <span style="color:#99cf50">extends</span> <span style="color:#9b5c2e;font-style:italic">Controller</span>
{
    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#dad085">__construct</span>()
    {
        <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>middleware(<span style="color:#65b042">'auth'</span>)<span style="color:#e28964">-></span>except([<span style="color:#65b042">'index'</span>,<span style="color:#65b042">'show'</span>]);
    }

    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">index</span>(<span style="color:#9b859d">Posts</span> <span style="color:#3e87e3">$posts</span>)
    {

        <span style="color:#3e87e3">$posts</span> <span style="color:#e28964">=</span> <span style="color:#9b859d">Post</span><span style="color:#e28964">::</span>latest()
            <span style="color:#e28964">-></span>filter(request([<span style="color:#65b042">'month'</span>, <span style="color:#65b042">'year'</span>]))
            <span style="color:#e28964">-></span>get();

<span style="color:#e28964">        return</span> view(<span style="color:#65b042">'posts.index'</span>, <span style="color:#dad085">compact</span>(<span style="color:#65b042">'posts'</span>));
    }

    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">show</span>(<span style="color:#9b859d">Post</span> <span style="color:#3e87e3">$post</span>)
    {
<span style="color:#e28964">        return</span> view(<span style="color:#65b042">"posts.show"</span>, <span style="color:#dad085">compact</span>(<span style="color:#65b042">'post'</span>));
    }

    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">create</span>()
    {
<span style="color:#e28964">        return</span> view(<span style="color:#65b042">"posts.create"</span>);
    }

    <span style="color:#99cf50">public </span><span style="color:#99cf50">function</span> <span style="color:#89bdff">store</span>()
    {
        <span style="color:#3e87e3">$this</span><span style="color:#e28964">-></span>validate(request(),[
            <span style="color:#65b042">'title'</span> <span style="color:#e28964">=></span> <span style="color:#65b042">'required'</span>,
            <span style="color:#65b042">'body'</span> <span style="color:#e28964">=></span> <span style="color:#65b042">'required'</span>
            ]);

        auth()<span style="color:#e28964">-></span>user()<span style="color:#e28964">-></span>publish(
            <span style="color:#e28964">new</span> <span style="color:#9b859d">Post</span>(request([<span style="color:#65b042">'title'</span>,<span style="color:#65b042">'body'</span>]))
        );

        session()<span style="color:#e28964">-></span>flash(<span style="color:#65b042">'message'</span>,<span style="color:#65b042">'Your post has now been published'</span>);

<span style="color:#e28964">        return</span> redirect(<span style="color:#65b042">'/'</span>);
    }
}

</pre>
`;